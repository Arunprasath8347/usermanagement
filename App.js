import React from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  Text,
  StatusBar,
} from 'react-native';
import UserNavigator  from './src/routes/UserNavigator';

function App() {
  return (    
      <UserNavigator />
  );
}

export default App;