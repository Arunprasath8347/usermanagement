import * as React from 'react';
//import { Text, View, StyleSheet, Image ,TextInput,TouchableOpacity} from 'react-native';
import { 
    View, 
    Text, 
    TouchableOpacity, 
    TextInput,
    Platform,
    StyleSheet ,
    StatusBar,
    Alert,
    ScrollView
} from 'react-native';
//import axios from 'axios'
//import * as Animatable from 'react-native-animatable';
//import LinearGradient from 'react-native-linear-gradient'
import FontAwesome from 'react-native-vector-icons/FontAwesome';
import Feather from 'react-native-vector-icons/Feather';
import { postMethod } from '../services/Apiservices';
import { useTheme } from 'react-native-paper';
class SignUp extends React.Component{
    state={
      firstname:"",
      lastname:"",
      username:"",
      password:"",
      emailid:"",
      phonenumber:""
    }
    onChangeHandle(state,value){
      this.setState({
        [state]:value
      })
    }
    doLogin(){
     const{firstname,lastname,username,password,emailid,phonenumber}=this.state;
      const req={
         "firstname":firstname,
         "lastname":lastname,
        "username":username,
        "password":password,
         "emailid":emailid,
         "phonenumber":phonenumber,
       
      }
      // axios.post("https://book-online-management.herokuapp.com//create_user/V1.0", req).then(
      //   res=>{
      //     this.props.navigation.navigate('SignIn');
      //     alert("Registered Sucessfully")
      //     console.log(res)
      //   },
      //   err=>{
      //     alert("wrong")
      //   }
      // )
      if (username != '') {
        //setIsLoading(true)
        postMethod("/school/signup", req)
            .then((response) => {
                if (response) {
                    console.warn("signup response", response);

                    if (response.status == 200) {

                    // const user_data = {
                    //         token: response.data.token,
                    //         userId: response.data.userId,
                    //         roles: response.data.roles,
                    //         userName: response.data.userName,
                    //     };

                        // setInfo(response)
                        // signIn(user_data);
                        // setIsLoading(false)

                        Alert.alert('user added successfully');
                    }
                   else if (response.status == 500) {
                    //setIsLoading(false)

                        Alert.alert('Not able to signup, Please try later');
                    }
                    if (response.statuscode == 404) {
                        //setIsLoading(false)

                        Alert.alert('User account already deactivated');
                    }
                }

            })
            .catch((error) => {
                //setIsLoading(false)

                Alert.alert('No Internet connection.\n Please check your internet connection \nor try again', error);
                console.warn('No Internet connection.\n Please check your internet connection \nor try again', error);
            });
    }
    else {
        //setIsLoading(false)

        Alert.alert('Username and Password cannot be empty');
    }
    }
  render(){
     const{firstname,lastname,username,password,emailid,phonenumber}=this.state;
    return(
      <ScrollView>
      <View style={styles.container}>
      <View style={styles.header}>
      <Text style={styles.text_header}>Register Now!</Text>
      </View>
      
      <View style={styles.footer}>
      <Text style={styles.text_footer}>First Name</Text>
        <View style={styles.action}>
        <FontAwesome name="user-o" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="enter first name" placeholderTextColor="#333"
      value={firstname} onChangeText={(value)=>this.onChangeHandle('firstname',value)}
       />
      </View>
      <Text style={styles.text_footer}>Last Name</Text>
      <View style={styles.action}>
      <FontAwesome name="user-o" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="Enter last name" placeholderTextColor="#333"
      value={lastname} onChangeText={(value)=>this.onChangeHandle('lastname',value)}
       />
      </View>

         <Text style={styles.text_footer}>Email</Text>
      <View style={styles.action}>
      <FontAwesome name="user-o" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="Enter email" placeholderTextColor="#333"
      value={emailid} onChangeText={(value)=>this.onChangeHandle('emailid',value)}
       />
      </View>
         <Text style={styles.text_footer}>UserName</Text>
       <View style={styles.action}>
       <FontAwesome name="user-o" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="Enter User name" placeholderTextColor="#333"
      value={username} onChangeText={(value)=>this.onChangeHandle('username',value)}
       />
      </View>
      <Text style={styles.text_footer}>Password</Text>
      <View style={styles.action}>
      <Feather name="lock" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="Enter Your Password"
      secureTextEntry={true} placeholderTextColor="#333"
      value={password} 
      onChangeText={(value)=>this.onChangeHandle('password',value) }
       />
       <Feather name="eye-off" color="grey" size={20}/>
      </View>
         <Text style={styles.text_footer}>phonenumber</Text>
      <View style={styles.action}>
      <FontAwesome name="user-o" color="#05375a" />
      <TextInput style={styles.textInput} placeholder="Role" placeholderTextColor="#333"
      value={phonenumber} onChangeText={(value)=>this.onChangeHandle('phonenumber',value)}
       />
      </View>
          
        <TouchableOpacity  style={styles.appButtonContainer}>
             <Text style={styles.appButtonText} secureTextEntry={true} color="grey" align="center" onPress={()=> this.doLogin()}>SignUp</Text>
           </TouchableOpacity>
           <Text>{}</Text>
           <Text>Already have an account please signin</Text> 
           <TouchableOpacity  style={styles.appButtonContainer}>
             <Text style={styles.appButtonText} secureTextEntry={true} color="grey" align="center"
              onPress={()=>this.props.navigation.navigate('login')}>SignIn</Text>
           </TouchableOpacity>             
      </View>
    
      </View>
  </ScrollView>
      // <View style={styles.container}>
      // <View style={styles.formWrapper}>
      // <Text style={styles.welcomeText}>SignUp</Text>
      
     
      // <TouchableOpacity style={styles.signinBtn}>
      // 
      // </TouchableOpacity>
      // </View>
      // </View>
    )
  }
}
export default SignUp;
const styles = StyleSheet.create({
    container: {
      flex: 1, 
      backgroundColor: '#fff'
    },
    header: {
        flex: 1,
        justifyContent: 'flex-end',
        paddingHorizontal: 20,
        paddingBottom: 50
    },
    footer: {
        flex: 3,
        backgroundColor: '#fff',
        borderTopLeftRadius: 30,
        borderTopRightRadius: 30,
        paddingHorizontal: 20,
        paddingVertical: 30
    },
    text_header: {
        color: '#0f73ee',
        fontWeight: 'bold',
        fontSize: 30
    },
    text_footer: {
        color: '#05375a',
        fontSize: 18
    },
    action: {
        flexDirection: 'row',
        marginTop: 10,
        borderBottomWidth: 1,
        borderBottomColor: '#f2f2f2',
        paddingBottom: 5
    },
    appButtonContainer: {
    elevation: 8,
    backgroundColor: "#0f73ee",
    borderRadius: 10,
    paddingVertical: 10,
    paddingHorizontal: 12
  },
  appButtonText: {
    fontSize: 18,
    color: "#fff",
    fontWeight: "bold",
    alignSelf: "center",
    textTransform: "uppercase"
  },
    // actionError: {
    //     flexDirection: 'row',
    //     marginTop: 10,
    //     borderBottomWidth: 1,
    //     borderBottomColor: '#FF0000',
    //     paddingBottom: 5
    // },
    textInput: {
        flex: 1,
        marginTop: Platform.OS === 'ios' ? 0 : -12,
        paddingLeft: 10,
        color: '#05375a',
    },
  //   signinBtn:{
  //     textAlign:"center",
  //     backgroundColor:"#6fbbd3",
  //     paddingVertical:10

  // },
  //   errorMsg: {
  //       color: '#FF0000',
  //       fontSize: 14,
  //   },
  //   button: {
  //       alignItems: 'center',
  //       marginTop: 50
  //   },
  //   signIn: {
  //       width: '100%',
  //       height: 50,
  //       justifyContent: 'center',
  //       alignItems: 'center',
  //       borderRadius: 10
  //   },
  //   textSign: {
  //       fontSize: 18,
  //       fontWeight: 'bold'
  //   }
  });
// const styles = StyleSheet.create({
//   container:{
//     height:"100%",
//     alignItems:"center",
//     justifyContent:"center"
//   },
//   formWrapper:{
//     width:"90%"
//   },
//   action:{
//    marginBottom: 10
//   },
//   textInput:{
//    backgroundColor:"#ddd",
//    height:40,
//    paddingHorizontal: 10,
//    color:"#333"
//   },
//   welcomeText:{
//           textAlign:"center",
//           marginBottom:30,
//           fontSize:24,
//           fontWeight:"bold"
//   },
//   signinBtn:{
//      backgroundColor:"blue",
//      paddingVertical:10

//   },
//   siginText:{
//      textAlign:"center",
//      color:"#fff",
//      fontSize:18,
//      fontWeight:"bold"

//   }
// })