import React from 'react';
import {createAppContainer} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Login from '../screens/Login';
import SignUp from '../screens/SignUp';


const UserNavigator = createStackNavigator(
    {
      signup: SignUp,
      login: Login,
    },   
    /*{
    defaultNavigationOptions: defaultStackNavOptions,
    },*/
  );

export default createAppContainer(UserNavigator);
